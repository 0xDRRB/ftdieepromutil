This is **not** a replacement for `ftdi_eeprom` command from [libFTDI](https://www.intra2net.com/en/developer/libftdi/).

It's just a funny tool to give funny names to funny devices, like this:

```
[28038.638867] usb 3-2.1.4.2: new full-speed USB device number 30 using xhci_hcd
[28038.781254] usb 3-2.1.4.2: New USB device found, idVendor=0403, idProduct=6001, bcdDevice= 6.00
[28038.781266] usb 3-2.1.4.2: New USB device strings: Mfr=1, Product=2, SerialNumber=0
[28038.781271] usb 3-2.1.4.2: Product: FT232R USB UART
[28038.781273] usb 3-2.1.4.2: Manufacturer: Hello ⭕⮜㉿⬟⏰⏻😊 lol
[28038.787162] ftdi_sio 3-2.1.4.2:1.0: FTDI USB Serial Device converter detected
[28038.787234] usb 3-2.1.4.2: Detected FT232R
[28038.795781] usb 3-2.1.4.2: FTDI USB Serial Device converter now attached to ttyUSB1
```

The USB specifications state that descriptors must be in UTF16 little endian, but there was no tool for storing UTF-16 strings in the EEPROM of FTDI FTxxxR components. Now there is.
