/*
 * Copyright (c) 2024 Denis Bodor
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIEDi
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <ctype.h>
#include <err.h>
#include <locale.h>
#include <langinfo.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <inttypes.h>
#include <libusb.h>
#include <iconv.h>
#include "color.h"

#define VID  0x0403
#define PID  0x6001

#define FTDI_DEVICE_IN_REQTYPE (LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_DEVICE | LIBUSB_ENDPOINT_IN)
#define FTDI_DEVICE_OUT_REQTYPE (LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_DEVICE | LIBUSB_ENDPOINT_OUT)
#define FTDI_EEPROM_SIZE       0x80
#define SIO_SET_LATENCY_REQ    0x09
#define SIO_READ_EEPROM_REQ    0x90
#define SIO_WRITE_EEPROM_REQ   0x91
#define SIO_ERASE_EEPROM_REQ   0x92
#define SIO_RESET_SIO          0x00
#define SIO_RESET_REQ          0x00
#define MAXSTR                 255
#define MAXTOTSTR              102

#define EN_SERIAL_LEAVE       -1
#define EN_SERIAL_OFF          0
#define EN_SERIAL_ON           1

libusb_context *ctx;
libusb_device_handle *handle;
int optdebug;
char *optmanufacturer;
char *optproduct;
char *optserial;

void printhelp(char *binname)
{
	printf("ftdieepromutil v0.1 - Copyright (c) 2024 - Denis Bodor\nThis is free software with ABSOLUTELY NO WARRANTY.\n");
	printf("Usage : %s [OPTIONS]\n", binname);
	printf(" -m \"manufacturer\"    set manufacturer string\n");
	printf(" -p \"product\"         set product string\n");
	printf(" -s \"serial\"          set serial string\n");
	printf(" -w                     write (default: display but not write)\n");
	printf(" -w                     write (default: display but not write)\n");
	printf(" -f                     force execution (ignore LC_TYPE != UTF-8)\n");
	printf(" -v                     be verbose\n");
	printf(" -d                     enable debug messages\n");
	printf(" -h                     show this help\n");
}

void printutf16(uint8_t *buf, size_t buflen)
{
	iconv_t utf16toutf8;
	int res;
	char dest[256] = { 0 }, *_dest = dest;
	size_t destlen = 255;

	if ((utf16toutf8 = iconv_open("UTF-8", "UTF-16LE")) == (iconv_t)-1)
		return;

	res = iconv(utf16toutf8, (char **)&buf, &buflen, &_dest, &destlen);
	if (res == 0) {
		printf("%s\n", dest);
	}

	if (iconv_close(utf16toutf8) != 0)
		return;
}

int eeprom2string(unsigned char *eeprombuf, int eepromsize, uint8_t stroffset, uint8_t lenoffset, char *dest, size_t *destlen)
{
	unsigned int offset = eeprombuf[stroffset] & (eepromsize -1);
	size_t len = (eeprombuf[lenoffset] - 2);

	if (!offset || !len)
		return (-1);

	unsigned char *_data = eeprombuf + offset + 2;
	// backup destination buffer length
	size_t startdestlen = *destlen;
	iconv_t utf16toutf8;
	int res;

	if ((utf16toutf8 = iconv_open("UTF-8", "UTF-16LE")) == (iconv_t)-1)
		return (-1);

	res = iconv(utf16toutf8, (char **)&_data, &len, &dest, destlen);
	if (res != 0)
		return (-1);

	if (iconv_close(utf16toutf8) != 0)
		return (-1);

	// return len of string instead of space left
	*destlen = startdestlen - *destlen;

	return (res);
}

int string2eeprom(char *src, size_t srclen, char *buf, size_t *buflen)
{
	int res;
	iconv_t utf8toutf16;
	size_t len = *buflen;

	if (!src || !srclen || !buf || !buflen)
		return (-1);

	if ((utf8toutf16 = iconv_open("UTF-16LE", "UTF-8")) == (iconv_t)-1)
		return (-1);

	res = iconv(utf8toutf16, (char **)&src, &srclen, &buf, &len);
	if (res != 0)
		return (-1);

	if (iconv_close(utf8toutf16) != 0)
		return (-1);

	*buflen = *buflen - len;
	return (0);

}

// from libftdi1-1.4
uint16_t makechsum(unsigned char *buf, size_t buflen)
{
	int i;
	unsigned short value, checksum = 0xAAAA;
	for (i = 0; i < buflen / 2 - 1; i++) {
		value = buf[i * 2];
		value += buf[(i * 2) + 1] << 8;
		checksum = value^checksum;
		checksum = (checksum << 1) | (checksum >> 15);
	}
	return (checksum);
}

void cleanup()
{
	if (optmanufacturer)
		free(optmanufacturer);
	if (optproduct)
		free(optproduct);
	if (optserial)
		free(optserial);
	if (handle)
		libusb_close(handle);
	if (ctx)
		libusb_exit(ctx);
}

int main (int argc, char**argv)
{
	libusb_device *dev;
	struct libusb_device_descriptor desc;
	unsigned char strdescr[128] = { 0 };
	int strdescrlen = 0;
	int ret;
	int retopt;
	int eepromsize = FTDI_EEPROM_SIZE;
	int i;
	uint8_t tmpbuf[256];
	unsigned char eeprombuf[256] = { 0 };

	// in EEPROM
	int manufacturer_offset;
	int manufacturer_len;
	int product_offset;
	int product_len;
	int serial_offset;
	int serial_len;

	// from EEPROM iconv transcoding
	char manufacturer[MAXSTR] = { 0 };
	size_t manufacturerlen = MAXSTR;
	char product[MAXSTR] = { 0 };
	size_t productlen = MAXSTR;
	char serial[MAXSTR] = { 0 };
	size_t seriallen = MAXSTR;

	unsigned short checksum;
	unsigned short usb_val;
	int optwrite = 0;
	int optverb = 0;
	int optenserial = EN_SERIAL_LEAVE;
	int optforce = 0;
	char *_optmanufacturer, *_optproduct, *_optserial;
	char newmanufacturer[MAXSTR] = { 0 };
	size_t newmanufacturerlen = MAXSTR;
	char newproduct[MAXSTR] = { 0 };
	size_t newproductlen = MAXSTR;
	char newserial[MAXSTR] = { 0 };
	size_t newseriallen = MAXSTR;

	int newmanufacturer_offset;
	int newproduct_offset;
	int newserial_offset;

	while ((retopt = getopt(argc, argv, "fwvdhm:p:s:e:")) != -1) {
		switch (retopt) {
			case 'v':
				optverb = 1;
				break;
			case 'd':
				optdebug = 1;
				optverb = 1;
				break;
			case 'w':
				optwrite = 1;
				break;
			case 'f':
				optforce = 1;
				break;
			case 'm':
				optmanufacturer = strdup(optarg);
				_optmanufacturer = optmanufacturer;
				break;
			case 'p':
				optproduct = strdup(optarg);
				_optproduct = optproduct;
				break;
			case 's':
				optserial = strdup(optarg);
				_optserial = optserial;
				break;
			case 'e':
				if (strncmp(optarg, "on", 2) == 0) {
					optenserial = EN_SERIAL_ON;
				} else if (strncmp(optarg, "off", 3) == 0) {
					optenserial = EN_SERIAL_OFF;
				} else {
					fprintf(stderr, "Invalid argument. Must be \"on\" or \"off\"\n");
					return(EXIT_FAILURE);
				}
				break;
			default:
				printhelp(argv[0]);
				return(EXIT_FAILURE);
		}
	}

	setlocale(LC_CTYPE, "");
	if (strncmp(nl_langinfo(CODESET), "UTF-8", 5) != 0) {
		if (optforce)
			warnx("Bad charset in terminal (must be \"UTF-8\")");
		else
			errx(EXIT_FAILURE, "Bad charset in terminal (must be \"UTF-8\")");
	}

	if ((ret = libusb_init(&ctx)) < 0)
		err(ret, "LibUSB initialisation error");

	if ((handle = libusb_open_device_with_vid_pid(ctx, VID, PID)) == NULL) {
		cleanup();
		errx(EXIT_FAILURE, "Unable to find device");
	}

	dev = libusb_get_device(handle);

	if ((ret = libusb_get_device_descriptor(dev, &desc)) < 0) {
		cleanup();
		err(ret, "failed to get device descriptor");
	}

	if (optverb) {
		printf("Info from libusb:\n");
		if ((strdescrlen = libusb_get_string_descriptor_ascii(handle, desc.iManufacturer, strdescr, 256)) > 0) {
			printf("  Manufacturer (ASCII): %s\n", strdescr);
			if ((ret = libusb_get_string_descriptor(handle, desc.iManufacturer, 0x0409, tmpbuf, 256)) > 0) {
				printf("              (UTF-16): ");
				printutf16(tmpbuf + 2, ret - 2);
				if (optdebug) {
					printf("    ");
					for (i = 0; i < ret; i++)
						printf("%02x ", tmpbuf[i]);
					printf("\n");
				}
			}
		}

		if ((strdescrlen = libusb_get_string_descriptor_ascii(handle, desc.iProduct, strdescr, 256)) > 0) {
			printf("  Product (ASCII): %s\n", strdescr);
			if ((ret = libusb_get_string_descriptor(handle, desc.iProduct, 0x0409, tmpbuf, 256)) > 0) {
				printf("         (UTF-16): ");
				printutf16(tmpbuf + 2, ret - 2);
				if (optdebug) {
					printf("    ");
					for (i = 0; i < ret; i++)
						printf("%02x ", tmpbuf[i]);
					printf("\n");
				}
			}
		}

		if ((strdescrlen = libusb_get_string_descriptor_ascii(handle, desc.iSerialNumber, strdescr, 256)) > 0) {
			printf("  Serial (ASCII): %s\n", strdescr);
			if ((ret = libusb_get_string_descriptor(handle, desc.iSerialNumber, 0x0409, tmpbuf, 256)) > 0) {
				printf("        (UTF-16): ");
				printutf16(tmpbuf + 2, ret - 2);
				if (optdebug) {
					printf("    ");
					for (i = 0; i < ret; i++)
						printf("%02x ", tmpbuf[i]);
					printf("\n");
				}
			}
		}

		printf("\n");
	}

	if (desc.bcdDevice != 0x600) {
		cleanup();
		errx(EXIT_FAILURE, "Not a FTDI *R chip. Abort.\n");
	}

	for (i = 0; i < eepromsize / 2; i++) {
		if (libusb_control_transfer(handle, FTDI_DEVICE_IN_REQTYPE, SIO_READ_EEPROM_REQ, 0, i, eeprombuf + (i * 2), 2, 2000) != 2) {
			cleanup();
			errx(EXIT_FAILURE, "reading eeprom failed!");
		}
	}

	manufacturer_offset = eeprombuf[0x0e] & (eepromsize - 1); // Addr 0E: Offset of the manufacturer string + 0x80
	manufacturer_len = eeprombuf[0x0f];
	product_offset = eeprombuf[0x10] & (eepromsize - 1); // Addr 10: Offset of the product string + 0x80, calculated later
	product_len = eeprombuf[0x11];
	serial_offset = eeprombuf[0x12] & (eepromsize - 1); // Addr 12: Offset of the serial string + 0x80, calculated later
	serial_len = eeprombuf[0x13];

	if((manufacturer_len + product_len + serial_len) > MAXTOTSTR) {
		cleanup();
		errx(EXIT_FAILURE, "Invalid strings size. Something's wrong!");
	}

	if (optverb) {
		printf("Current EEPROM content:\n");
		for (i = 0; i < eepromsize; i++) {
			if(!(i%16)) printf("%02x: ", i);

			if (i == 0x0e || i == 0x0f ) printf(YELLOW);
			if (i == 0x10 || i == 0x11 ) printf(GREEN);
			if (i == 0x12 || i == 0x13 ) printf(CYAN);
			if (manufacturer_len && i >= manufacturer_offset && i < manufacturer_offset + manufacturer_len) printf(BOLDYELLOW);
			if (product_len && i >= product_offset && i < product_offset + product_len) printf(BOLDGREEN);
			if (serial_len && i >= serial_offset && i < serial_offset + serial_len) printf(BOLDCYAN);
			if (i == 0x0a) printf(BOLDBLUE);

			printf("%02x" RESET " ", eeprombuf[i]);

			if(!((i + 1) % 16)) printf("\n");
		}

		checksum = makechsum(eeprombuf, eepromsize);
		printf("checksum: %02x%02x", (uint8_t)checksum, (uint8_t)(checksum >> 8));
		if (eeprombuf[eepromsize - 2] == (uint8_t)checksum && eeprombuf[eepromsize - 1] == (uint8_t)(checksum >> 8))
			printf(BOLDGREEN " OK" RESET "\n");
		else
			printf(BOLDRED " BAD" RESET "\n");

		printf(BOLDYELLOW "manufacturer" RESET " string [0x%02x] @ 0x%02x (len=%u)\n", eeprombuf[0x0e], manufacturer_offset, manufacturer_len);
		printf(BOLDGREEN "product" RESET " string [0x%02x] @ 0x%02x (len=%u)\n", eeprombuf[0x10], product_offset, product_len);
		printf(BOLDCYAN "serial" RESET " string  [0x%02x] @ 0x%02x (len=%u)\n", eeprombuf[0x12], serial_offset, serial_len);
		printf("\n");
	}

	if (optverb) {
		printf("Current strings from EEPROM:\n");
		if (eeprom2string(eeprombuf, eepromsize, 0x0e, 0x0f, manufacturer, &manufacturerlen) != 0) {
			warn("iconv error (manufacturer)");
		} else {
			printf("  Manufacturer: \"%s\" (%zu)\n", manufacturer, manufacturerlen);
		}

		if (eeprom2string(eeprombuf, eepromsize, 0x10, 0x11, product, &productlen) != 0) {
			warn("iconv error (product)");
		} else {
			printf("  Product:      \"%s\" (%zu)\n", product, productlen);
		}

		if (eeprom2string(eeprombuf, eepromsize, 0x12, 0x13, serial, &seriallen) != 0) {
			warn("iconv error (serial)");
		} else {
			printf("  Serial:       \"%s\" (%zu) %s\n", serial, seriallen, eeprombuf[0x0a] & (1 << 3) ? "used" : "not used");
		}
		printf("\n-----\n\n");
	}

	// strings update
	if (optmanufacturer || optproduct || optserial || optenserial >= 0) {
		// process new string or recycle the old one
		if (optmanufacturer) {
			if (string2eeprom(_optmanufacturer, strlen(optmanufacturer), newmanufacturer, &newmanufacturerlen) < 0) {
				cleanup();
				errx(EXIT_FAILURE, "Error in UTF16 conversion");
			}
		} else {
			newmanufacturerlen = manufacturer_len - 2;
			memcpy(newmanufacturer, eeprombuf + manufacturer_offset + 2, newmanufacturerlen);
		}
		if (optdebug) {
			printf("New Manufacturer (UTF16): ");
			for (i = 0; i < newmanufacturerlen; i++)
				printf("%02x ", (uint8_t)newmanufacturer[i]);
			printf(" (%zu)\n", newmanufacturerlen);
		}

		// process new string or recycle the old one
		if (optproduct) {
			if (string2eeprom(_optproduct, strlen(optproduct), newproduct, &newproductlen) < 0) {
				cleanup();
				errx(EXIT_FAILURE, "Error in UTF16 conversion");
			}
		} else {
			newproductlen = product_len - 2;
			memcpy(newproduct, eeprombuf + product_offset + 2, newproductlen);
		}
		if (optdebug) {
			printf("New Product (UTF16): ");
			for (i = 0; i < newproductlen; i++)
				printf("%02x ", (uint8_t)newproduct[i]);
			printf(" (%zu)\n", newproductlen);
		}

		// process new string or recycle the old one
		if (optserial) {
			if (string2eeprom(_optserial, strlen(optserial), newserial, &newseriallen) < 0) {
				cleanup();
				errx(EXIT_FAILURE, "Error in UTF16 conversion");
			}
		} else {
			newseriallen = serial_len - 2;
			memcpy(newserial, eeprombuf + serial_offset + 2, serial_len);
		}
		if (optdebug) {
			printf("New Serial (UTF16): ");
			for (i = 0; i < newseriallen; i++)
				printf("%02x ", (uint8_t)newserial[i]);
			printf(" (%zu)\n", newseriallen);
		}

		// check available space
		if (newmanufacturerlen + 2 + newproductlen + 2 + newseriallen + 2 > MAXTOTSTR) {
			cleanup();
			errx(EXIT_FAILURE, "New strings too big!");
		}

		memset(eeprombuf + 0x18, 0x00, eepromsize - 0x18);

		// compute offsets and copy data
		newmanufacturer_offset = 0x18;
		memcpy(eeprombuf + newmanufacturer_offset + 2, newmanufacturer, newmanufacturerlen);
		newproduct_offset = newmanufacturer_offset + 2 + newmanufacturerlen;
		memcpy(eeprombuf + newproduct_offset + 2, newproduct, newproductlen);
		newserial_offset = newproduct_offset + 2 + newproductlen;
		memcpy(eeprombuf + newserial_offset + 2, newserial, newseriallen);

		// set offsets and size
		eeprombuf[newmanufacturer_offset] = newmanufacturerlen + 2;
		eeprombuf[newmanufacturer_offset + 1] = 0x03;
		eeprombuf[newproduct_offset] = newproductlen + 2;
		eeprombuf[newproduct_offset + 1] = 0x03;
		eeprombuf[newserial_offset] = newseriallen + 2;
		eeprombuf[newserial_offset + 1] = 0x03;

		// set bLength and type
		eeprombuf[0x0e] = newmanufacturer_offset | 0x80;
		eeprombuf[0x0f] = newmanufacturerlen + 2;
		eeprombuf[0x10] = newproduct_offset | 0x80;
		eeprombuf[0x11] = newproductlen + 2;
		eeprombuf[0x12] = newserial_offset | 0x80;
		eeprombuf[0x13] = newseriallen + 2;

		// enable/disable serial usage
		if (optenserial == EN_SERIAL_ON)
			eeprombuf[0x0a] |= (1 << 3);
		if (optenserial == EN_SERIAL_OFF)
			eeprombuf[0x0a] &= ~(1 << 3);

		// add checksum
		checksum = makechsum(eeprombuf, eepromsize);
		eeprombuf[eepromsize - 2] = (uint8_t)checksum;
		eeprombuf[eepromsize - 1] = (uint8_t)(checksum >> 8);

		if (optverb) {
			printf("New EEPROM content:\n");
			for (i = 0; i < eepromsize; i++) {
				if(!(i%16)) printf("%02x: ", i);

				if (i == 0x0e || i == 0x0f ) printf(YELLOW);
				if (i == 0x10 || i == 0x11 ) printf(GREEN);
				if (i == 0x12 || i == 0x13 ) printf(CYAN);
				if (newmanufacturerlen && i >= newmanufacturer_offset && i < newmanufacturer_offset + newmanufacturerlen + 2) printf(BOLDYELLOW);
				if (newproductlen && i >= newproduct_offset && i < newproduct_offset + newproductlen + 2) printf(BOLDGREEN);
				if (newseriallen && i >= newserial_offset && i < newserial_offset + newseriallen + 2) printf(BOLDCYAN);
				if (i == 0x0a) printf(BOLDBLUE);

				printf("%02x" RESET " ", eeprombuf[i]);

				if(!((i + 1) % 16)) printf("\n");
			}
			printf(BOLDYELLOW "manufacturer" RESET " string [0x%02x] @ 0x%02x (len=%zu)\n", eeprombuf[0x0e], newmanufacturer_offset, newmanufacturerlen + 2);
			printf(BOLDGREEN "product" RESET " string [0x%02x] @ 0x%02x (len=%zu)\n", eeprombuf[0x10], newproduct_offset, productlen + 2);
			printf(BOLDCYAN "serial" RESET " string  [0x%02x] @ 0x%02x (len=%zu)\n", eeprombuf[0x12], newserial_offset, seriallen + 2);
			printf("\n");
		}
	}

	if (optwrite) {
		// reset before write eeprom - from libftdi1-1.4
		if (libusb_control_transfer(handle, FTDI_DEVICE_OUT_REQTYPE, SIO_RESET_REQ, SIO_RESET_SIO, 0, NULL, 0, 5000) < 0) {
			cleanup();
			err(EXIT_FAILURE, "Error reseting device!");
		}

		// Set latency timer before write eeprom - from libftdi1-1.4
		if (libusb_control_transfer(handle, FTDI_DEVICE_OUT_REQTYPE, SIO_SET_LATENCY_REQ, (unsigned short)0x77, 0, NULL, 0, 5000) < 0) {
			cleanup();
			err(EXIT_FAILURE, "Error reseting device!");
		}

		if (optverb)
			printf("Writing EEPROM...\n");
		for (i = 0; i < eepromsize / 2; i++) {
			usb_val = eeprombuf[i * 2];
			usb_val += eeprombuf[(i * 2) + 1] << 8;
			if (libusb_control_transfer(handle, FTDI_DEVICE_OUT_REQTYPE, SIO_WRITE_EEPROM_REQ, usb_val, i, NULL, 0, 5000) < 0) {
				cleanup();
				err(EXIT_FAILURE, "writing eeprom failed!");
			}
			if (optdebug) {
				printf("%04x ", usb_val);
				if(!((i + 1) % 8))
					printf("\n");
			} else if (optverb) {
				printf("% 3d%%\r", (i + 1) * 100 / (eepromsize / 2));
				fflush(stdout);
			}
		}
		if (optverb)
			printf("\ndone.\n");
	}

	cleanup();
	return(EXIT_SUCCESS);
}

